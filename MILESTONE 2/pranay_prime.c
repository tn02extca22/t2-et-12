#include <stdio.h>
int main()
{
    int x, i, flag = 0;
    printf("Enter a positive integer: ");
    scanf("%d", &x);
    for(i = 2; i <= x/2; ++i)
    {

        if(x%i == 0)
        {
            flag = 1;
            break;
        }
    }
    if (x == 1)
    {
      printf("1 is neither a prime nor a composite number.");
    }
    else
    {
        if (flag == 0)
          printf("1");
        else
          printf("0");
    }

    return 0;
}
